package Test3.question3;
import javafx.event.*;
import javafx.scene.control.*;
import javafx.*;
/*
 * this is for displaying a message to the user 
 * about whether their bet is valid or not.
 * 
 */
public class Messages implements EventHandler<ActionEvent> {
	private BetGame game;
	private TextField betting;
	private TextField moneyAmount;
	private int userChoice;
	private TextField message;
	/*
	 * constructor
	 */
	public Messages(BetGame game,TextField betting,TextField moneyAmount,int userChoice,TextField message) {
		this.game=game;
		this.betting=betting;
		this.moneyAmount=moneyAmount;
		this.userChoice=userChoice;
		this.message=message;
	}
	/*
	 * for the GUI!
	 */
	@Override
	public void handle(ActionEvent action) {
	String bet = betting.getText();
	int betNum=Integer.valueOf(bet);
	if(this.game.betValidation(betNum)==true) {
		this.message.setText("Your bet is valid! YAY!");
	}
	else {
		this.message.setText("Your bet is invalid! NO!");
	}
	this.game.betting(betNum, this.userChoice);
	//Adding a space so this could be seen as a String.
	this.moneyAmount.setText(""+this.game.getMoney());
	}
}
