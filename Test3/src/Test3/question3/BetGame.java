package Test3.question3;
import javafx.*;
import javafx.scene.control.*;
import javafx.event.*;
import java.util.*;
/*
 * this is the backend part of Question 3.
 * the user always start with 500 money
 * enter via a textField of how much they wish to bet for a specific round
 * if the user places an invalid bet, then there should be no change.
 * a message should indicate "NOT VALID."
 * if the bet is valid, simulate a roll of a die, which has a 50 percent chance
 * of being small and a 50% being large, use the random class to generate this.
 * If they are correct, they gain the same amount of the money they bet.
 * If they are incorrect, they lose the same amount of money they bet.
 * must display what happened in each round and how much money the user still has.
 */
public class BetGame {
	//two fields to record the results.
	private int money;
	private Random r = new Random();
	
	//constructor
	public BetGame() {
		//A user should always start with 500!
		this.money=500;
	}
	public int rollingDice() {
		//Since there is no 0 on dice, we add 1 to make up.
		int result=r.nextInt(6)+1;
		return result;
		
	}
	public int getMoney() {
		return this.money;
	}
	/*
	 * this method validates the bet!
	 */
	public boolean betValidation(int betAmount) {
		if(betAmount==0 || betAmount> this.money) {
			return false;
		}
		return true;
	}
	/*
	 * this method determines if the user has won their prize or not.
	 */
	public void betting(int betAmount, int userChoice) {
		//0 represents small, 1 represents large.
		//anything >3 is consider an acceptable result for large.
		if(userChoice==1) {
			if(betValidation(betAmount)==true) {
				int roll=rollingDice();
				if(roll>3) {
					this.money +=betAmount;
				}
				else {
					this.money -=betAmount;
				}
			}
		}
		else if (userChoice==0) {
			if(betValidation(betAmount)) {
				int roll= rollingDice();
				if(roll<=3) {
					this.money +=betAmount;
				}
				else {
					this.money -=betAmount;
				}
			}
		}
		
	}
}
