package Test3.question3;
import javafx.application.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.*;
/*
 * the GUI part of the question.
 */
public class UserInterface extends Application{
	private BetGame game = new BetGame();
	private int bet;
	@Override
	public void start(Stage s){
		Group root= new Group();
		VBox general = new VBox();
		Scene scene = new Scene(root,800,500);
		scene.setFill(Color.BLACK);
		Button small = new Button("Small");
		Button large = new Button("Large");
		TextField moneyAmount= new TextField("500");
		TextField betting = new TextField(""+bet);
		TextField message= new TextField("You will see validation message here!");
		general.getChildren().addAll(small,large,moneyAmount,betting,message);
		root.getChildren().addAll(general);
		Messages beOntSmall = new Messages(game, moneyAmount,betting,0,message);
		Messages betOnBig = new Messages(game,moneyAmount,betting,1,message);
		small.setOnAction(betOnSmall);
		large.setOnAction(betOnBig);
		s.setTitle("Betting on large or small dice rollings!");
		s.setScene(scene);
		s.show();
	}
	public static void main(String[] args) {
		launch(args);
	}

}
