/**
 * 
 */
package Test3;

/**
 * @author Louisa Yuxin Lin 1933472
 *
 */
public class Recursion {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] numbers = new int[] {40,23,45,10};
		int result= recuriseCount(numbers,10);
		System.out.println(result);
	}
	/*
	 * this method should return the number of integers in the 
	 * number array that are >20 and located at an odd index
	 */
	public static int recuriseCount(int[] numbers,int n) {
		int count=0;
		//make sure n is not negative or 0!
		if(n<=0) {
			for(int i=0; i<numbers.length; i++) {
				if(numbers[i]>20|| i%2==0|| numbers[i]>=n) {
				count++;
			}
			}
		}
			else {
				return count;
			}
			
			return 0;
		}
	}


